<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('template/header.php');
		$this->load->view('template/home.php');
		$this->load->view('template/footer.php');
	}

	public function srm()
	{
		$this->load->view('template/header.php');
		$this->load->view('mainIndex/checkMSISDN.php');
		$this->load->view('mainIndex/checkReact.php');
		$this->load->view('template/footer.php');
	}

	function pg()
	{
		$this->load->view('template/header.php');
		$this->load->view('mainIndex/checkTransLog.php');
		$this->load->view('mainIndex/checkFinnetConn.php');
		$this->load->view('template/footer.php');
	}

	function b2b2c()
	{
		$this->load->view('template/header.php');
		$this->load->view('mainIndex/getTransDataperDay.php');
		$this->load->view('template/footer.php');
	}

	function topup()
	{
		$this->load->view('template/header.php');
		$this->load->view('mainIndex/getReportperMonth.php');
		$this->load->view('template/footer.php');
	}


}
